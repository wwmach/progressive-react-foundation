const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require("path");

const babelOptions = {
  "presets": [
    "react",
    "es2015"
  ]
}

module.exports = {
  context: __dirname,
  entry: {
    main: '../client/index.tsx'
  },
  target: 'web',
  output: {
    path: path.resolve('./bin/'),
    filename: "[name].js"
  },
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js'],
  },
  module: {
    loaders: [
      {test: /\.css$/, loader: ExtractTextPlugin.extract({fallback: "style-loader", use: "css-loader"})},
      {test: /\.s(a|c)ss$/, loader: ExtractTextPlugin.extract({fallback: "style-loader", use: ["css-loader", "postcss-loader", "typed-css-modules-loader", "sass-loader"]})},
      {test: /\.ts(x?)$/, loaders: ['babel-loader', 'ts-loader']},
      {test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff"},
      {test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader"},
      {test: /\.(jpg|ico|png|gif|eot|ttf|svg)$/, loader: 'url-loader?limit=100000'}
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        "BUILD_TARGET": JSON.stringify("client")
      }
    }),
    new ExtractTextPlugin("[name].css"),
    // new webpack.optimize.CommonsChunkPlugin({

    // })
  ]
}