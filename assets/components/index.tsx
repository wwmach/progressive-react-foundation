import * as React from 'react';

import '../scss/global.scss';

export class App extends React.Component<{}, {}> {
  render(): JSX.Element {
    return <div>Hello World</div>
  }
}