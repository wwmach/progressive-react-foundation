import * as React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { App } from '../assets/components/index';

declare var module;

function rerender() {
  render(
    <AppContainer>
      <App />
    </AppContainer>
    , document.getElementById('root'));
}

  if (module.hot) {
    module.hot.accept('../assets/components/index', rerender)
  }

  rerender();