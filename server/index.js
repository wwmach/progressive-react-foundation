import http from 'http';
import app from './server';

const server = http.createServer(app);
server.listen(3000);

if (module.hot) {
  let currentApp = app;
  module.hot.accept('./server', () => {
    server.removeListener('request', currentApp);
    server.on('request', app);
    currentApp = app;
  });
}