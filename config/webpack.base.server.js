const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const StartServerPlugin = require('start-server-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

module.exports = {
  entry: [
    './server/index.js',
  ],
  target: 'node',
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js'],
  },
  module: {
    loaders: [
      {test: /\.css$/, loader: ExtractTextPlugin.extract({fallback: "style-loader", use: "css-loader"})},
      {test: /\.s(a|c)ss$/, loader: ExtractTextPlugin.extract({fallback: "style-loader", use: ["css-loader", "postcss-loader", "typed-css-modules-loader", "sass-loader"]})},
      {test: /\.ts(x?)$/, loaders: ['babel-loader', 'ts-loader']},
      {test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff"},
      {test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader"},
      {test: /\.(jpg|ico|png|gif|eot|ttf|svg)$/, loader: 'url-loader?limit=100000'}
    ]
  },
  externals: [nodeExternals()],
  plugins: [
    new ExtractTextPlugin("[name].css"),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      "process.env": {
        "BUILD_TARGET": JSON.stringify('server')
      }
    }),
  ],
  output: {
    path: path.resolve('./bin/'),
    filename: 'server.js'
  }
}