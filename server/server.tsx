import * as express from 'express';
import compression from 'compression';
import { renderToString } from 'react-dom/server';
import * as React from 'react';

import { App } from '../assets/components/index';

declare var process;

const app = express();
let scripts;
let styles;

if (process.env.NODE_ENV === 'production') {
  scripts = `
    <script defer type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/react/15.4.2/react.min.js'></script>
    <script defer type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/react/15.4.2/react-dom.min.js'></script>
    <script defer type='text/javascript' src='/assets/main.js'></script>
  `
  styles = `
    <link rel='stylesheet' type='text/css' href='/assets/main.css'>
  `
} else {
  styles = '<link rel="stylesheet" type="text/css" href="http://localhost:3001/main.css">';
  scripts = '<script src="http://localhost:3001/main.js"></script>'
}

app.get('*', (req, res) => {
  let application = renderToString(<App />);
  let html = `
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="UTF-8">
        <title>title</title>
        ${styles}
      </head>
      <body>
      <div id='root'>${application}</div>
      ${scripts}
      </body>
    </html>
  `
  res.send(html);
})

export default app;